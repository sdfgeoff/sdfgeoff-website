import os
import json
import shutil


def parse(text):
    content, metadata = parse_metadata(text)
    content = remove_escaped(content)

    content = replace_headings(content)
    content = replace_bullets(content)
    content, images = replace_images(content)
    content = replace_links(content)

    content = replace_paragraphs(content)
    format_code()
    content = replace_escaped(content)
    #content = generate_toc(content)
    
    resources = images

    return content, metadata, resources



toc_str = ''
prev_level = 1
def register_heading(level, name):
    pass
    # ~ global toc_str, prev_level
    # ~ #Registers a name for the TOC
    # ~ if level > prev_level:
        # ~ toc_str += '<ul>\n'
        # ~ prev_level = level
    # ~ elif level < prev_level:
        # ~ toc_str += '</ul>'
        # ~ prev_level = level
    
    # ~ toc_str += '<li><a href="#'+name+'">'+name+'</a></li>\n'


# ~ def generate_toc(content):
    # ~ #generates a table of contents
    # ~ if content.find('<toc>') == -1:
        # ~ return content
    
    # ~ num_in = (toc_str.match(/<ul>/g)or[]).length
    # ~ num_out = (toc_str.match(/<\/ul>/g)or[]).length
    # ~ for i=0; i < num_in - num_out; i++:
        # ~ toc_str += '</ul>'
    
    # ~ toc_str = '<div class="toc">' + toc_str + '</div>'
    # ~ content = content.replace('<toc>', toc_str)
    # ~ return content


def replace_bullets(content):
    bul = get_between(content, '\n-', '\n\n')
    while bul != -1:
        new_bul = bul.replace('\n-', '<ul>\n    <li>',1)
        new_bul = new_bul.replace("\n-", '</li>\n    <li>')
        new_bul = new_bul.strip()
        new_bul += '</li>\n</ul>'
        content = content.replace(bul, new_bul)
        bul = get_between(content, '\n-', '\n\n')
    
    num = get_between(content, '\n# ', '\n\n')
    while num != -1:
        new_num = num.replace('\n#', '<ol><li>')
        new_num = new_num.replace("/\n#/g", '</li><li>')
        new_num += '</li></ol>'
        content = content.replace(num, new_num)
        num = get_between(content, '\n#', '\n\n')
    
    return content


def parse_metadata(text):
    title = get_between(text, 'Title:', '\n')
    author = get_between(text, 'Author:', '\n') 
    date = get_between(text, 'Date:', '\n')
    edit = get_between(text, 'Edit:', '\n')
    
    d, m, y = date.strip().split(" ")[-1].split("-")
    date2 = "{}-{}-{}".format(y, m, d)
    
    if edit == -1:
        edit = ""

    text = text.replace(title, '').replace(author, '').replace(date, '').replace(edit, '').strip()

    if edit == -1:
        edit = ''
    
    if title != -1:
        title = title[6: -1]
    else:
        title = 'Untitled'
        
    metadata = json.dumps({
        "tags": [],
        "date": date2,
        "content": ["main.html"]
    }, indent=4)
    

    home = ""#HOME_STR
    metablock = author + '<br>' + date + '<br>' + edit + '<br>'
    metablock = '<div class=metablock>'+metablock+'</div>'
    link_str = '<div class=links>'+home+metablock+'</div>'
    

    text = '<h1>'+title+'</h1>\n\n' + text + "\n\n"#+link_str + text
    return text, metadata


def replace_paragraphs(text):
    par = get_between(text, '\n\n', '\n\n')
    while par != -1:
        body = par[2: -2]
        text = text.replace(par, '\n<p>\n'+body+'\n</p>\n\n')
        par = get_between(text, '\n\n', '\n\n')
    
    return text


def replace_newlines(text):
    #while text.find('\n') != -1:
    text = text.replace('\n', '</br>\n')
    
    return text


def replace_headings(text):
    heading = get_between(text, '\n=','\n')
    while heading != -1:
        heading_level = str(len(heading.split('=')) - 1)
        headingtext = "".join(heading.split('='))[1:-1].strip()
        register_heading(heading_level, headingtext)
        headingtext = '\n \n<h'+heading_level+'>'+headingtext+'</h'+heading_level+'>\n '
        text = text.replace(heading, headingtext)
        heading = get_between(text, '\n=','\n')
    
    return text


def replace_links(text):
    linkdata = get_between(text, '[', ']')
    while linkdata != -1:
        link = parse_link_sections(linkdata, True)
        linkaddr = link[0]
        linktext = link[1]
        heading = get_between(linktext, 'title="', '"')
        if heading != -1 and heading != 'title=""':
            heading = heading[7:-1]
            newlinkstr = linktext+'<a href="'+linkaddr+'" class=label>'+heading+'</a>'
        else:
            newlinkstr = '<a href="'+linkaddr+'">'+linktext+'</a>'
        
        text = text.replace(linkdata, newlinkstr)
        linkdata = get_between(text, '[', ']')
    
    return text


def replace_images(text):
    linkdata = get_between(text, '![', ']')
    images = []
    while linkdata != -1:
        link = parse_link_sections(linkdata)
        linkaddr = link[0]
        linktext = link[1]
        images.append(linkaddr)
        newlinkstr = '<img src="'+linkaddr+'" title="'+linktext+'"></img>'
        if text[text.find(linkdata)-1] != '[':
            newlinkstr = '<a href="'+linkaddr+'">'+newlinkstr+'</a>'
        
        text = text.replace(linkdata, newlinkstr)

        linkdata = get_between(text, '![', ']')
    
    return text, images


def parse_link_sections(linkdata, page=None):
    #Takes a string and separates it into an address and a name
    first = linkdata.find('[')
    if linkdata.find(' ') != -1:
        linkaddr = linkdata.split(' ')[-1:][0][0: -1]
        linktext = " ".join(linkdata.split(' ')[0:-1])[first+1:]

    else:
        linkaddr = linkdata[linkdata.find('[')+1:-1]
        linktext = ''
    
    if linkaddr[0:4] != 'http':
        #Is a local link
        path_text = ""#window.location.search
        path_text = path_text.replace('%2F', '/')
        folder = "/".join(path_text[1:].split('/')[0:-1])
        if linkaddr.find('../') != -1:
            folder = "/".join(folder.split('/')[0:-1])
            linkaddr = linkaddr.replace('../', '')
        
        if folder != '':
            folder += '/'
        
        linkaddr = folder+linkaddr
        if page == True and linkaddr.find('.') == -1:
            linkaddr = 'index.html?' + linkaddr
        
    
    return [linkaddr, linktext]


escaped = {}
code = {}
min_id = 10000000
def remove_escaped(text):
    #Removes any code blocks or any escaped control symbols from the
    #text. They are added back in later
    
    global escaped, code, min_id
    escaped = {}
    code = {}
    min_id = 10000000
    
    
    co = get_between(text, '\n{\n', '\n}\n')
    
    
    while co != -1:
        joiner = '!'+str(min_id)+'!'
        code[joiner] = co[3:-2]
        text = text.replace(co, joiner)
        min_id += 1
        co = get_between(text, '\n{\n', '\n}\n')
    
    loc = text.find('\\')
    while loc != -1:
        text_to_protect = text[loc+1]
        joiner = '!'+str(min_id)+'!'
        text = text[0:loc] + joiner + text[loc+2:]
        min_id += 1
        escaped[joiner] = text_to_protect
        loc = text.find('\\', loc+1)
    

    return text


def replace_escaped(text):
    for esc in escaped:
        assert text.find(esc) != -1
        text = text.replace(esc, escaped[esc])
    
    for co in code:
        assert text.find(co) != -1
        text = text.replace(co, code[co])
    
    return text


def replace_nbsp(text):
    index = text.find(' ')
    while index != -1:
        text = text.replace(' ', '&nbsp')
        index = text.find(' ')
    
    return text


def format_code():
    global code
    
    for co in code:
        #d = document.createElement('div')
        #d.appendChild(document.createTextNode(code[co]))
        #code[co] = co#d.innerHTML
        #code[co] = replace_nbsp(code[co])
        #code[co] = replace_newlines(code[co])
        code[co] = '\n<code>\n'+code[co]+'</code>\n\n'

def get_between(text, start, end, index_from=None):
    #Returns the text (start and end inclusive) between two
    #start and end characters, eg:
    #   get_between("tes<asdf>t", "<", ">"
    #will return:
    #   "<asdf>"
    #Which a string.repalce can deal with nicely....
    
    start_index = text.find(start, index_from)
    end_index = text.find(end, start_index+len(start))
    if start_index == -1 or end_index == -1:
        return -1
    else:
        return text[start_index: end_index+len(end)]



for filename in os.listdir():
    if filename != "generate.py" and os.path.isfile(filename):
        
        #if filename != "simplegame":
        #    continue
        
        print(filename)
        raw = open(filename).read()
        formatted, metadata, resources = parse(raw)
        
        folder = "../" +filename
        if not os.path.isdir(folder):
            os.mkdir(folder)
        open(folder + "/main.html", "w").write(formatted)
        open(folder + "/info.json", "w").write(metadata)
        
        for resource in resources:
            outpath = os.path.join(folder, resource)
            out_folder = os.path.dirname(outpath)
            if not os.path.isdir(out_folder):
                os.mkdir(out_folder)
            shutil.copy(resource, outpath)
        
        (formatted)
