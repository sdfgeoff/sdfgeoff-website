Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2015-11-21T16:52:00+13:00

====== gamedata ======
Created Saturday 21 November 2015

The gamedata dict is sent from a client to the tracker when they host a game. It contains the following:
**Property Name		Description**
level				The level name
comment			A user specified comment about the game
name				Pilot Name

==== Potential Properties: ====
tracked				True if the game is tracked - this is not seen by the server, only seen client side
