precision highp float;

        uniform sampler2D uDiffuseMap;

        varying vec2 to_frag_UV;
        varying float to_frag_grad;


        void main(void)
        {
            vec4 col = texture2D(uDiffuseMap, to_frag_UV);
            col = col/to_frag_grad;
            //col.a = col.r/to_frag_grad * 2.0;
            col.g *= 0.4;
            col.b *= 0.2;
            col.r *= 0.6;
            
            
            
            gl_FragColor = col;
        }
