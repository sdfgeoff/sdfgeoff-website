//Contains a function for spawning ships, as well as the internal
//ship functions

requireobj('SixShip');
requirejs('laser.js');
requirejs('missile.js');
requirejs('powerup.js');
requirejs('common.js');
requiresound('Laser');
requiresound('Thrust');

SPEED = 6;
ACCEL = 2; //So it accelerates in ~1/3 second

TURN = 2.5;
TACCEL = TURN/3;

EM_THRUST_RATE = 2
EM_THRUST_CONSUME = 100/30 //energy per second

CAMERA_POS = [0, 0.0925, -0.0055]

SHIELD = 100.0
REGEN_RATE = 0.3

MAX_MISSILES = 1
MISSILE_ENERGY = 2;    //Energy cost per missile
MISSILE_FIRE_RATE = 0.8  //Can fire one missile every n seconds
MISSILE_REGEN_RATE = 0.1 //One missile every n seconds

LASER_POS_LIST = [
    //left, up, forwards
    [ 0.2662,-0.1467,0.5],
    [-0.2662,-0.1467,0.5],
    [-0.3182,0.11229,0.5],
    [ 0.3182,0.11229,0.5],
]
MISSILE_POS_LIST = [
    [0,-0.15,0.15],
]

function ship(){
    //Class for a ship
}



ship.prototype.shootPrimary = function(player){
    pos = this.obj.getPosition();
    rot = this.obj.getEulerAngles();

    //Bottom Lasers          left    up     forwards
    for (pos_offset in LASER_POS_LIST){
        shootLaser(pos, rot, LASER_POS_LIST[pos_offset], player);
    }
    this.obj.audiosource.play('Laser'+SOUND_FORMAT)
}

missile_pos = 0
ship.prototype.shootSecondary = function(player){
    pos = this.obj.getPosition();
    rot = this.obj.getEulerAngles();


    if (missile_pos+1 > MISSILE_POS_LIST.length-1){
        missile_pos = 0
    } else {
        missile_pos += 1
    }
    pos_offset = MISSILE_POS_LIST[missile_pos];
    shootMissile(pos, rot, pos_offset, player);
}

ship.prototype.init = function(pos, player){
    //Creates a ship at the speficied location
    pobj = new pc.Entity();
    app.systems.model.addComponent(pobj, {
        type: "asset",
        asset: getObj('SixShip.json', "model")
    });
    pobj.translate(pos[0]);
    pobj.rotate(pos[1])
    pobj.addComponent('collision', {type:'sphere', radius:0.5});

    if (player == true){ //It's the player, collide with enemy weapons, walls and powerups
        pobj.addComponent('rigidbody', {type:'dynamic',
                                        mass:1.0,
                                        friction:0,
                                        group: PLAYER_GROUP,
                                        mask: (pc.BODYGROUP_STATIC | DRONE_GROUP | DRONE_WEAP_GROUP | POWERUP_GROUP)
                                        });
    } else { //It's a drone, collide with player weapons, powerups

        pobj.addComponent('rigidbody', {type:'dynamic',
                                        mass:10.0,
                                        friction:0,
                                        group: DRONE_GROUP,
                                        mask: (pc.BODYGROUP_STATIC  | PLAYER_GROUP | PLAYER_WEAP_GROUP | POWERUP_GROUP)
                                        });
    }

    app.root.addChild(pobj);
    pobj.name = "Ship"


     //Sound:
    pobj.addComponent("audiosource", {
        assets: [getObj('Laser'+SOUND_FORMAT)],
    });
    pobj.audiosource.stop()

    //The engine sound:
    this.engine = new pc.Entity()
    //this.engine.translate(pos[0]);
    pobj.addChild(this.engine)

    this.engine.addComponent("audiosource", {
        assets: [getObj('Thrust'+SOUND_FORMAT)],
        loop:true,
        volume:0,
    });
    this.thrustOn = false
    this.obj = pobj;
}

ship.prototype.applyForce = function(forc){
    this.obj.rigidbody.applyForce(forc);
}
ship.prototype.addHealth = function(health){
    this.energy += health
}

ship.prototype.manageThrustSound = function(enabled){
    this.thrustOn = (this.thrustOn * 0.9 + enabled*0.1)
    this.engine.audiosource.pitch = this.thrustOn*1.5 + 1
    this.engine.audiosource.volume = this.thrustOn+0.2
}

function spawnShip(pos, player){
    //Spawns in a ship at the specified location
    s = new ship();//
    s.init(pos, player);

    //Spawn Effect:
    //????

    return s;
}
continueloading=true
