PLAYER_GROUP = pc.BODYGROUP_USER_1
DRONE_GROUP = pc.BODYGROUP_USER_2
DRONE_WEAP_GROUP = pc.BODYGROUP_USER_3
PLAYER_WEAP_GROUP = pc.BODYGROUP_USER_4
POWERUP_GROUP = pc.BODYGROUP_USER_5



function glob2loc(vec, entity){
    lvec = new pc.Vec3()
    lvec.x = vec.dot(entity.right)
    lvec.y = vec.dot(entity.forward)
    lvec.z = vec.dot(entity.up)
    return lvec
}

function loc2glob(vec, entity){
    gvec = new pc.Vec3()
    gvec.add(entity.right.clone().scale(vec.x))
    gvec.add(entity.forward.clone().scale(vec.y))
    gvec.add(entity.up.clone().scale(vec.z))
    return gvec
}

function clamp(num, mi, ma){
    if (num > ma){
        return ma
    } else if (num < mi){
        return mi
    } else {
        return num
    }
}

function clampVecSphere(vec, ma){
    nvec = vec.clone()
    if (nvec.length > ma){
        nvec.length = mx
    }
    return nvec
}
function clampVecCube(vec, ma){
    nvec = vec.clone()
    nvec.x = clamp(nvec.x, -ma, ma)
    nvec.y = clamp(nvec.y, -ma, ma)
    nvec.z = clamp(nvec.z, -ma, ma)
    return nvec
}
continueloading = true


function makeObj(asset_name, pos, rot){
    e = new pc.Entity
    app.systems.model.addComponent(e, {
        type: "asset",
        asset: getObj(asset_name+'json', "model")
    });

    if (pos != undefined){
        console.log(rot)
        e.translate(pos);
    }
    if (rot != undefined){
        e.rotate(rot);
    }
    app.root.addChild(e);

    return e;
}


function getVectBetween(ob1, ob2){
    dist = ob1.getPosition().sub(ob2.getPosition())
    return dist
}

function getRandomVect(scale){
    vec = new pc.Vec3(Math.random()-0.5, Math.random()-0.5, Math.random()-0.5)
    vec.scale(scale*2)
    return vec
}


// The following function is from stackoverflow. I don't really know how colorspace works
function hslToRgb(h, s, l){

	var hue2rgb = function hue2rgb(p, q, t){
		if(t < 0) t += 1;
		if(t > 1) t -= 1;
		if(t < 1/6) return p + (q - p) * 6 * t;
		if(t < 1/2) return q;
		if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
		return p;
	}

	var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
	var p = 2 * l - q;
	r = hue2rgb(p, q, h + 1/3);
	g = hue2rgb(p, q, h);
	b = hue2rgb(p, q, h - 1/3);

    return [r, g, b];
}


function vecToList(vec){
    return [vec.x , vec.y, vec.z]
}

function listToVec(lis){
    return new pc.Vec3(lis[0], lis[1], lis[2])
}


function checkLOS(entity1, entity2){
    //Returns true if there is line of site between entity1 and entity2
    hasLOS = true
    app.systems.rigidbody.raycastFirst(entity1.position, entity2.position, function (result) {
        hasLOS = false;
    });
    return hasLOS
}
