//////////// Proportional Controller ////////////
var P = function(kp){
	this.kp = kp
}

P.prototype.update = function(cur, tar, dt) {
    tar = tar.clone()
    cur = cur.clone()
	dif = tar.sub(cur)
	pval = dif.scale(this.kp)
	return pval
}

var PID = function(kp, ki, kd){
    this.kp = kp
    this.ki = ki
    this.kd = kd
    this.prev_val = 0;
}

PID.prototype.update = function(cur, tar, dt) {
    error = cur - tar
    deriv = this.prev_val - error
    this.prev_val = error
	return error*this.kp + deriv*this.kd
}


continueloading=true

