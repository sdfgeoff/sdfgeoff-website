function createCookie(name,value,days) {
	localStorage[name] = value
}

function readCookie(name) {
    if (name in localStorage){
        return localStorage[name]
    } else {
        return null
    }
}

function eraseCookie(name) {
	localStorage.removeItem(name)
}
continueloading=true
