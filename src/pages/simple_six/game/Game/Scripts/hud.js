requirejs('svg.min.js');
requirejs('task.js');
requirejs('control.js');

page_w = window.innerWidth
page_h = window.innerHeight

//energyText = null;
//missileLockText = null;
//emergencyThrustText = null;
HUDHealth = 0
//HUDFilter = null;

energyBar = null;
missileReadyLight = null;
EMLight = null;
missileLockLight = null;

deathCount = null
killCount = null
timerDisplay = null

bigMessage = null
missileLockedOn = false

enterText = false

chatBox = null;

textBox = document.createElement('input')
textBox.className = "invisible"
document.getElementById("HUD").appendChild(textBox)

function initHUD(){
    addTask(updateHUD, 0.1, -1);
    
    draw = SVG('HUD_SVG').size(page_w, page_h);

    deathCount = draw.text('0').move(page_w/2+page_h*0.392, page_h*(0.793)).fill('#FFF').skew(12, 0)
    deathCount.font({
        family: 'simpsix',
        size: page_h/30,
        anchor: 'left',
    })
        
    killCount = draw.text('0').move(page_w/2 +page_h*0.385, page_h*(0.76)).fill('#FFF').skew(12, 0)
    killCount.font({
        family: 'simpsix',
        size: page_h/30,
        anchor: 'left',
    })
        
    timerDisplay = draw.text('0').move(page_w/2 +page_h*0.405, page_h*(0.865)).fill('#FFF').skew(12, 0)
    timerDisplay.font({
        family: 'simpsix',
        size: page_h/30,
        anchor: 'left',
    })
    
    
    bigMessage = draw.text('').move(page_w/2 , page_h/2).fill('#fff')
    bigMessage.font({
        family: 'simpsix',
        size: page_h/10,
        anchor: 'middle',
    })
    
    chatBox =  draw.text('').move(page_w/2 - page_h*0.35 , page_h*0.85).fill('#fff')
    chatBox.font({
        family: 'simpsix',
        size: page_h/35,
        anchor: 'left',
    })
    
}

function updateHUD(dt){
    if (playerObj == null){
        //Player hasn't spawned or is dead
        HUDHealth = 0
        killCount.text('')
        deathCount.text('')
        timerDisplay.text('')
        bigMessage.text('Press '+KEYS['SForwards']+' To Spawn')
        
        document.getElementById("HUD").style.backgroundColor = "rgba(0,0,0,255)"
        return
    }
    
    updateCockpitObjects()
    bigMessage.text('')
    
	energyBar.node.setLocalScale(new pc.Vec3(Math.min(1,playerObj.energy/100),1,1))
	
	updateMissileLight()
	updateEMLight()
	updateLockLight()
	updateColorOverlay()
    updateCockpitText()
    
    playerChat()
}

function playerChat(){
    if (enterText == true){
        if (getKey('submit') == true){
            enterText = false
            gameLog(PILOT['pilotname']+': '+textBox.value)
            sendChatMessage(textBox.value.substring(1)) 
        }
        if (textBox.value == ''){
            textBox.value == '>'
        }
        textBox.focus()
        chatBox.text(textBox.value)
    } else {
        if (getKey('chat') == true){
            enterText = true
        }
        if (playerObj != null){
            chatBox.text('Press 8 to chat')
            textBox.value = '>'
        } else {
            chatBox.text('')
        }
    }
}

function gameLog(msg){
    console.log(msg)
}

function updateCockpitText(){
    deathCount.text('Deaths: '+player_deaths.toString())
    killCount.text('Kills: '+player_kills.toString())

    seconds = Math.floor(tim%60).toString()
    if (seconds.length == 1){
        seconds = '0'+seconds
    }
    timerDisplay.text('Time: '+Math.floor(tim/60).toString()+':'+seconds)
}

function updateColorOverlay(){
    HUDHealth *= 0.8;
    if (HUDHealth > 0){
		col = "rgba(0,255,255,<>)"
	} else {
		col = "rgba(255,0,0,<>)"
	}
    opacity = Math.min(Math.abs(HUDHealth/6), 0.5);
    document.getElementById("HUD").style.backgroundColor = col.replace("<>", opacity);
}

function updateLockLight(){
    if (missileLockedOn == true){
		missileLockLight.material.opacity = 0.0
	} else {
		missileLockLight.material.opacity = 0.8
	}
	missileLockLight.material.update()
    missileLockedOn = false
}


function updateCockpitObjects(){
    for (me in playerObj.obj.model.model.meshInstances){
        oName = playerObj.obj.model.model.meshInstances[me].node.name
        if (oName == 'Plane'){
            energyBar = playerObj.obj.model.model.meshInstances[me]
        } else if (oName == 'Plane.002'){
            missileReadyLight = playerObj.obj.model.model.meshInstances[me]
        } else if (oName == 'Plane.001'){
            EMLight = playerObj.obj.model.model.meshInstances[me]
        } else if (oName == 'Plane.003'){
            missileLockLight = playerObj.obj.model.model.meshInstances[me]
        }
    }
}

function updateEMLight(){
    if (playerObj.EMThrust == true){
		EMLight.material.opacity = 0.0
	} else {
		EMLight.material.opacity = 0.8
	}
	EMLight.material.update()
}

function updateMissileLight(){
    if (playerObj.secReady == true){
		missileReadyLight.material.opacity = 0.0
	} else {
		missileReadyLight.material.opacity = 0.8
	}
	missileReadyLight.material.update()
}

function addHUDHealth(qty){
	HUDHealth += qty;
}

function missileLock(){
    missileLockedOn = true
}

continueloading = true;
