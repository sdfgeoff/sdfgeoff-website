requirejs('playcanvas-stable.min.js')
requirejs('ammo.js')
requirejs('task.js')
requirejs('control.js')
requirejs('player.js')
requirejs('level.js')
requirejs('drone.js')


mouse = null
keyboard = null

playerObj = null // The current player object
gameState = initstate //The current 'state' of the game [spawn, play, dying, dead, menu]


function startGame(){
    keyboard = new pc.Keyboard(document.body)
    mouse = new pc.Mouse(document.body)
    
    app.start()
    
    console.log('Game Started')
    
    //Remove loading division:
    l = document.getElementById("loadstate");
    l.parentNode.removeChild(l);
    
    app.on("update", function (dt) {
        updateTasks(dt);
        gameState(dt);
    })
    
}

function initstate(dt){
    initMulti()
    initControl()
    initLevel()
    app.systems.rigidbody.setGravity(0, 0, 0);
    initHUD();
    gameState = spawnstate
    console.log("Game Initialized")
}



function spawnstate(dt){
    if (level_data_loaded == false){
        console.log("Leveldata not loaded yet (oops)")
        return;
    }
    if(getKey('SForwards')){
        //Spawn the player
        console.log("(re)Spawning Player")
        playerObj = spawnPlayer(getSpawnPoint())
        gameState = playstate
    }
}

function playstate(dt){
    
}

continueloading = true
