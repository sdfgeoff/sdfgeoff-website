requirejs('common.js')
requirejs('explosion.js')
requireobj('Missile')
requiresound('Missile')

MISSILE_SPEED = 10;

MISSILE_MAX_TORQUE = 5
MISSILE_PROPORTIONAL = 25
MISSILE_DERIVATIVE = 0.07
MAX_SENSE_ANGLE = 1.0
MISSILE_SPIN_RATE = 1000


function missile(){
    //Class for a laser bolt
}

missile.prototype.collide = function(result){
    placeExplosion(this.entity.getPosition());
    this.entity.destroy();
    endTask(this.entity.task);
}

missile.prototype.init = function(pos, rot, pos_offset, player){
    mobj = new pc.Entity();
    app.systems.model.addComponent(mobj, {
        type: "asset",
        asset: getObj('Missile.json', "model")
    });
    mobj.translate(pos);
    mobj.rotate(rot);
    mobj.translateLocal(pos_offset[0], pos_offset[1], pos_offset[2]);
    mobj.addComponent('collision', {type:'capsule',
                                    radius:0.08,
                                    height:0.6,
                                    axis:2});

    if (player == true){ //Shot by player: collide with Drones
        mobj.target = 'Drone'
        mobj.addComponent('rigidbody', {type:'dynamic',
                                        group: PLAYER_WEAP_GROUP,
                                        mask: (pc.BODYGROUP_STATIC | DRONE_GROUP)
                                        });
    } else { //Shot by drone: collide with player
        mobj.target = 'Player'
        mobj.addComponent('rigidbody', {type:'dynamic',
                                        group: DRONE_WEAP_GROUP,
                                        mask: (pc.BODYGROUP_STATIC | PLAYER_GROUP)
                                        });
    }
    mobj.rigidbody.syncEntityToBody();
    app.root.addChild(mobj);


    mobj.rigidbody.linearVelocity = loc2glob(new pc.Vec3(0,-MISSILE_SPEED,0), mobj);
    //mobj.rigidbody.angularVelocity = loc2glob(new pc.Vec3(0,100,0), mobj);
    mobj.rigidbody.angularDamping = 1
    mobj.collision.on('collisionstart', this.collide);

    mobj.addComponent("audiosource", {
        assets: [getObj('Missile'+SOUND_FORMAT)],
        loop:true,
        volume:0.6,
        pitch:0.7,
    });
    m.PID = new PID(MISSILE_PROPORTIONAL, 0, MISSILE_DERIVATIVE)
    m.last_dir = null
    this.obj = mobj;
}
missile.prototype.home = function(dt, m){
    tar = null
    if (m.obj.target == 'Drone'){
        tar = droneObj
    } else if (m.obj.target == 'Player'){
        tar = playerObj
    }
    m.obj.rigidbody.linearVelocity = loc2glob(new pc.Vec3(0,-MISSILE_SPEED,0), m.obj);
    if (tar != null && checkLOS(m.obj, tar.obj) == true){


        tar_dir = new pc.Vec3();
        tar_dir.sub2(m.obj.position, tar.obj.position)
        if (m.last_dir == null ){
            m.last_dir = tar_dir;
            return;
        }

        dir = new pc.Vec3()
        dir.cross(m.last_dir, tar_dir)

        tar_dir.normalize()
        ang = Math.acos(m.obj.forward.dot(tar_dir))
        if (ang > MAX_SENSE_ANGLE){ //Target at too sharp-er angle
            return
        }

        torque_magnitude = m.PID.update(dir.length(), 0, dt)
        torque_magnitude = clamp(torque_magnitude, -MISSILE_MAX_TORQUE,MISSILE_MAX_TORQUE)

        torque = dir.normalize()
        torque.scale(torque_magnitude)

        m.obj.rigidbody.applyTorque(torque)
        m.last_dir = tar_dir


        if (tar == playerObj){
            missileLock()
        }
    }

}


function shootMissile(pos, rot, pos_offset, player){
    //Spawns a laser at the specified position vector
    m = new missile();
    m.init(pos, rot, pos_offset, player);
    m.obj.task = addTask(m.home, 0, -1, m);
    return m
}


continueloading=true;
