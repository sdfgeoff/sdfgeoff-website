/*This file contains a function for loading other files
The requirejs is a little like #include from C
It also implements requireobj(url, folder). If folder is blank it assumes asset folder
requiresound and requireshader are also implemented now.
*/

app = null
REVISION = Math.floor(Math.random()*10000)

//----------------------------------------------------------------------
js_load_state = 0
js_load_num = 0
js_load_list = []
js_loaded_list = []


function requirejs(url){
    //Will load the javascript at specified URL, and any it requires etc.
    if ((js_load_list.indexOf(url) == -1) && (js_loaded_list.indexOf(url) == -1)){ //Haven't already loaded it
        js_load_list.push(url)
        if (js_load_state == 0){
            loadjs()
            js_loaded_list.push(url)
        }
    }

}

function loadjs(){
    if (js_load_state == 0){
        //Update the display
        loaderdisplay('Loading Script '+js_load_num)
        //Reschedule this function giving time for display to update
        js_load_state++
        js_load_num ++
        setTimeout(loadjs, 0.1)
    } else if (js_load_state == 1){
        //Load the javascript
        continueloading = false
        url = js_load_list.shift()
        head = document.getElementsByTagName('head')[0]
        script = document.createElement('script')
        script.type = 'text/javascript'
        script.src = 'Scripts/'+url+'?v='+REVISION //The oldest javascript file
        js_loaded_list.push(url)
        head.appendChild(script)
        head.removeChild(script)
        js_load_state++
        setTimeout(loadjs, 0.1)

    } else if (js_load_state == 2){
        //check if it's loaded
        if (continueloading == true){
            js_load_state = 0
            if (js_load_list.length > 0){
                loadjs()
            } else {
                finishedjsload()
            }
        } else {
            setTimeout(loadjs, 0.1)
        }
    }
}

function loaderdisplay(text){
    //Displays the specified text in the loading section of the HTML file
    load_box = document.getElementById("loadstate")
    if (load_box != null){
        load_box.innerHTML = text
    }
}

function finishedjsload(){
    loaderdisplay('')
    console.log('Scripts Loaded')
    setTimeout(loadobj,0.1);
}

//----------------------------------------------------------------------

requiredobjects = []
loadedobjects = {}
loadstate = 0

function requireobj(url, folder){
    if (typeof(folder) === 'undefined') {
        folder = 'Assets/Models/'
    }
    //Will load the specified object before the game loads
    requiredobjects.push([url, folder])
}

function loadobj(){
    //Load all the playcanvas objects
    if (loadstate == 0){
        loaderdisplay('Loading Objects')
        canvas = document.getElementById("application-canvas")
        app = new pc.Application(canvas)
        app.setCanvasFillMode(pc.FILLMODE_FILL_WINDOW)
        app.setCanvasResolution(pc.RESOLUTION_AUTO)
        setTimeout(loadobj, 0.1);
        loadstate ++;
    }
    else {

        for (var i = 0; i < requiredobjects.length; i++) {
            url = requiredobjects[i][1]+'/'+ requiredobjects[i][0] + '.json?'+REVISION
            app.assets.loadFromUrl(url, "model", function (err, asset) {
                loadstate++;
                //console.log('loaded: '+asset.name)
                if (loadstate === requiredobjects.length+1) {
                    loaderdisplay('');
                    setTimeout(loadSounds, 0.1);
                } else {
                    loaderdisplay('Loading Object '+loadstate+'/'+requiredobjects.length);
                }
            });
        }
        //setTimeout(checkLoadObjDone, 0.1);
    }
}

function checkLoadObjDone(){
    num_failed = 0
    for (var i = 0; i < requiredobjects.length; i++) {
        if (app.assets.find(requiredobjects[i][0] + '.json?'+REVISION, "model") == undefined){
            num_failed += 1
        }
    }
    if (num_failed == 0){
        setTimeout(loadSounds, 0.1);
    } else {
        setTimeout(checkLoadObjDone, 0.1);
    }
}

function getObj(assetName){
    res = app.assets.find(assetName + "?" + REVISION);
    return res
}
//----------------------------------------------------------------------

requiredshaders = []
loadedshaders = {}
loadshadernum = 0
loadshaderstate = 0

fragtext = ''
vertext = ''
function requireshader(name){
    requiredshaders.push(name);
}

function loadShaders(){
    if (loadshadernum == requiredshaders.length){
        loaderdisplay('');
        startGame();
        loadshadernum += 1; //To stop double execution of startGame, which was happening for some reason.
    } else if (loadshadernum < requiredshaders.length) {
        if (loadshaderstate == 0){
            loaderdisplay('Loading Shader '+loadshaderstate+'/'+requiredshaders.length);
            name = requiredshaders[loadshadernum]
            fpath = 'Shaders/'+name+'.f.glsl';
            vpath = 'Shaders/'+name+'.v.glsl';

            frag = new XMLHttpRequest();
            frag.open("GET", fpath, true);
            frag.onload = function(){
                fragtext = this.responseText;
                loadshaderstate += 1;
                setTimeout(loadShaders, 0.1);
            }
            frag.send(null);

            vert = new XMLHttpRequest();
            vert.open("GET", vpath, true);
            vert.onload = function(){
                verttext = this.responseText;
                loadshaderstate += 1;
                setTimeout(loadShaders, 0.1);
            }
            vert.send(null);
        } else if (loadshaderstate == 2){

            shaderDefinition = {
                attributes: {
                    aPosition: pc.SEMANTIC_POSITION,
                    aNormal: pc.SEMANTIC_NORMAL,
                    aUv0: pc.SEMANTIC_TEXCOORD0
                },
                vshader: verttext,
                fshader: fragtext
            };

            loadedshaders[requiredshaders[loadshadernum]] = shader = new pc.Shader(app.graphicsDevice, shaderDefinition);
            verttext = '';
            fragtext = '';
            loadshaderstate = 0;
            loadshadernum += 1;
            setTimeout(loadShaders, 0.1);
        }
    }
}

function getShader(name){
    return loadedshaders[name];
}

//----------------------------------------------------------------------
requiredsounds = []
loadedsounds = {}
soundloadstate = 0


if (navigator.userAgent.indexOf('Firefox') > -1){
    //Use .ogg
    SOUND_FORMAT = '.ogg'
} else {
    SOUND_FORMAT = '.mp3'
}

function requiresound(url){
    requiredsounds.push(url)
}

function loadSounds(){
    for (var i = 0; i < requiredsounds.length; i++) {
        url = 'Assets/Sounds/'+requiredsounds[i] + SOUND_FORMAT
        //console.log(url)
        app.assets.loadFromUrl(url, "audio", function (err, asset) {
            soundloadstate++;
            //console.log('loaded: '+asset.name)
            if (soundloadstate === requiredsounds.length) {
                loaderdisplay('');
                setTimeout(loadShaders, 0.1);
            } else {
                loaderdisplay('Loading Sound '+soundloadstate+'/'+requiredsounds.length);
            }
        });
    }
    //setTimeout(loadShaders, 0.1)
}
