server = "ws://"+"pi.sdfgeoff.space"+":"+42425;

tracker = null
tracker_msg_box = null

tracker_list_callback = null
//function trackerError()

function connectToTracker(msg){
    //Pass in three functions:
    // The first will report the status of the server
    tracker_msg_box = msg
    tracker_msg_box("Connecting to Tracker")

    tracker = new WebSocket(server);

    tracker.onopen = trackerOpen
    tracker.onerror = trackerError
    tracker.onmessage = trackerHandler
    tracker.onclose = trackerClose
    tracker.messageHandler = messageHandler
}

function trackerError(error){
    tracker_msg_box("Connection to Tracker Failed")
    tracker = null
}

function trackerOpen(){
    tracker_msg_box("Connected to Tracker")
}

function trackerClose(){
    tracker_msg_box("Connection to Tracker Lost")
    tracker = null
}

function trackerHandler(m){
    if (m.data.slice(0,4) == 'LIST'){
        tracker_list_callback(JSON.parse(m.data.slice(4)))
    } else {
        tracker.messageHandler(m)
    }
}

function messageHandler(m){
    console.log(m.data)
}

function getTrackerGameList(fun){
    //Runs the passed in function with a json dictionary of games
    if (fun){
        tracker_list_callback = fun
    }
    if (tracker == null){
        connectToTracker(tracker_msg_box)
        setTimeout(getTrackerGameList, 10)
    } else if (tracker.readyState == 0){
        setTimeout(getTrackerGameList, 10)
    } else if (tracker.readyState == 1){
        tracker.send("LIST")
    }
}

function print(data){
    console.log(data)
}

gametrackerdata = null
function hostTrackerGame(){
    if (gametrackerdata == null){
        gametrackerdata = JSON.parse(atob(self.location.search.slice(1))) //Remove first '?'
    }

    if (tracker == null){
        connectToTracker(print)
        setTimeout(hostTrackerGame, 1000)
    } else if (tracker.readyState == 0){
        setTimeout(hostTrackerGame, 10)
    } else if (tracker.readyState == 1){
        gametrackerdata['state'] = multistate
        //console.log('HOST'+JSON.stringify(gametrackerdata))
        tracker.send('HOST'+JSON.stringify(gametrackerdata))
        setTimeout(hostTrackerGame, 10000)
    }


}

function initTrackerHost(){
    hostTrackerGame()

}

continueloading = true
