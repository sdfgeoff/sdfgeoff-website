function loadJson(url, func){
    //Loads the requested URL and runs the supplied function when loaded
    var req = new XMLHttpRequest();

    req.onreadystatechange = function() {
        if (req.readyState == 4 && req.status == 200) {
            var myArr = JSON.parse(req.responseText);
            
            func(myArr);
        }
    }
    req.open("GET", url, true);
    req.send();
}

continueloading=true
