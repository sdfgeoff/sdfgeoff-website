requirejs('pid.js');
requirejs('ship.js');
requirejs('hud.js');
requirejs('task.js');
requirejs('common.js');

player_deaths = 0
player_kills = 0

function updateControl(dt, dict){
    //Takes a dictionary of form: {action:percent}
    if (dict['Em'] == 1 && this.energy > dt*EM_THRUST_CONSUME){
        scalar = EM_THRUST_RATE
        this.energy -= dt*EM_THRUST_CONSUME
        this.EMThrust = true
        this.manageThrustSound(true)
    } else {
        scalar = 1
        this.EMThrust = false
        this.manageThrustSound(false)
    }
    
    if (dict['Pri'] == 1 && this.energy > LASER_ENERGY && this.pri_timer >= LASER_FIRE_TIME){
        this.energy -= LASER_ENERGY;
        this.shootPrimary(true);
        this.pri_timer = 0;
        sendPrimaryShootEvent();
    }
    if (this.missiles >= 1 && this.sec_timer >= MISSILE_FIRE_RATE){
		this.secReady = true
		if (dict['Sec'] == 1){
			this.missiles -= 1
            this.shootSecondary(true);
            this.sec_timer = 0;
            sendSecondaryShootEvent();
		}
	} else {
		this.secReady = false
	}

    this.pri_timer += dt;
    this.sec_timer += dt;
    //this.energy += dt/5
    

    this.mvec.y = -dict['Sf']
    this.mvec.z = dict['Su']
    this.mvec.x = dict['Ss']
    this.mvec.scale(SPEED*scalar)

    cm = glob2loc(this.obj.rigidbody.linearVelocity, this.obj)
    this.mvec = this.m_pid.update(cm, this.mvec, dt)
    
    tim += dt
    this.mvec.z += 2*Math.sin(tim * 6.28)
    this.mvec = loc2glob(this.mvec, this.obj)      
    
    this.rvec.y = dict['Roll']
    this.rvec.z = dict['Pan']
    this.rvec.x = dict['Tilt']
    
    this.rvec.scale(TURN*scalar)
    cr = glob2loc(this.obj.rigidbody.angularVelocity, this.obj)
    this.rvec = this.r_pid.update(cr, this.rvec, dt)
    
    this.rvec = loc2glob(this.rvec, this.obj)
    this.obj.rigidbody.applyTorque(this.rvec)
    this.obj.rigidbody.applyForce(this.mvec)
}
tim = 0
function updatePlayer(dt){
    //Checks if dead, gradual increase in energy etc.
    if (playerObj.missiles < MAX_MISSILES && playerObj.energy >= MISSILE_ENERGY){
        playerObj.missiles += dt / MISSILE_REGEN_RATE
        playerObj.energy -= dt / MISSILE_REGEN_RATE * MISSILE_ENERGY
    }
    if (playerObj.energy <= 0){
        playerObj.die()
    } else if (playerObj.energy < 100){
        playerObj.energy += dt*REGEN_RATE
    }

    
}

function initPlayer(ship){
    //Turns a ship into a player
    
    //Logic:
    ship.control = addTask(controlUpdate, 0, -1);
    ship.update = addTask(updatePlayer, 0, -1)
    
    ship.obj.collision.on('collisionstart', playerCollision);
    
    //Ship variables:
    ship.energy = SHIELD;
    ship.missiles = MAX_MISSILES;
    
    ship.m_pid = new P(ACCEL); //Proportional Controller
    ship.r_pid = new P(TACCEL);
    ship.mvec = new pc.Vec3()
    ship.rvec = new pc.Vec3()
    
    //Bind in functions and variables:
    ship.updateControl = updateControl
    ship.pri_timer = 0;
    ship.sec_timer = 0;
    
    //Create the camera:
    camera = new pc.Entity();
    camera.addComponent("camera", {
        clearColor: new pc.Color(0, 0, 0),
        nearClip: 0.01,
        fov: 110/2
    });
    camera.rotate(0, 180, 0);
    camera.translate(CAMERA_POS[0], CAMERA_POS[1], CAMERA_POS[2]);
    ship.obj.addChild(camera);
    ship.cam = camera;
    
    //Create ship color:
    col = hslToRgb(PILOT['color']/360, 1, 0.5)
    for (me in ship.obj.model.model.meshInstances){
		matName = ship.obj.model.model.meshInstances[me].material.name
		if (matName == 'PlayerColor.json' || matName == 'Reticle.json'){
			mat = ship.obj.model.model.meshInstances[me].material.clone()
			ship.obj.model.model.meshInstances[me].material = mat
			mat.diffuse = new pc.Color(col[0], col[1], col[2])
            
            if (matName == 'Reticle.json'){
                mat.emissive = mat.diffuse
            }
			mat.update()
		}
	}
    
    //Sound:
    ship.cam.addComponent("audiolistener");
}

function playerCollision(result){
    if (result.other.name == "Powerup"){
        playerObj.addHealth(POWERUP_HEALTH)
    } else if (result.other.name == "Laser"){
		playerObj.addHealth(-LASER_DAMAGE)
	}
}

function playerAddHealth(health){
	addHUDHealth(health)
    sendHitEvent(health)
    this.energy += health
}

function spawnPlayer(pos){
    //Starts spawn animation etc.
    //Returns the new player
    p = new ship();//
    p.init(pos, true);

    p.addHealth = playerAddHealth
    p.die = playerDie

    initPlayer(p);
    
    return p;   
}

function playerDie(){
    ///Spawns powerups and removes the ship entities:
    v1 = getRandomVect(10)
    v2 = getRandomVect(10)
    v3 = getRandomVect(10)
    makePowerup(this.obj.getPosition(), v1);
    makePowerup(this.obj.getPosition(), v2);
    makePowerup(this.obj.getPosition(), v3);
    
    pos = vecToList(playerObj.obj.getPosition())
    
    
    playerObj.obj.destroy()
    playerObj.engine.destroy()
    
    endTask(playerObj.control)
    endTask(playerObj.update)
    
    playerObj = null
    placeExplosion(listToVec(pos)) //Has to be after playerObj is nulled to stop explosion force being applied to nonexistant ship.
    
    gameState = spawnstate
    
    player_deaths += 1
    sendDeathEvent(player_deaths, pos, [vecToList(v1), vecToList(v2), vecToList(v3)])
    
    
}

function getMData(){
    d = {
        'p':vecToList(playerObj.obj.getPosition()),
        'v':vecToList(playerObj.obj.rigidbody.linearVelocity),
        'r':vecToList(playerObj.obj.getEulerAngles()),
        'a':vecToList(playerObj.obj.rigidbody.angularVelocity),
        'e':playerObj.EMThrust,
    }
    
    return d
}

continueloading=true;
