// ------------- Level Loading ------------------

//Note the requireobj for the level is in host.html and multi.js depending on if the player is hosting
requirejs('json.js')
LEVEL_DATA = null
level_data_loaded = false
function initLevel(){
    //Place Level:
    level_data_loaded = false
    loadJson('Levels/'+level_name+'/leveldata.json', levelDataLoaded)

    //Place the level object, which we don't need levelData for

    level = new pc.Entity()
    app.systems.model.addComponent(level, {
       type: "asset",
       asset: getObj(level_name+'.json')
     });
    level.addComponent('collision', {
        type:'mesh'
    });
    level.collision.model = getObj(level_name+'.json').resource

    level.addComponent('rigidbody', {type:'static'})

    app.root.addChild(level)
}

function levelDataLoaded(d){
    LEVEL_DATA = d
    level_data_loaded = true

    //Place the lights:
    light_list = LEVEL_DATA['lamps']
    for (light in light_list){
        addLight(light_list[light])
    }

}

function getSpawnPoint(){
    //Returns a random spawn point that it got from the leveldata file
    spawns = LEVEL_DATA['empties']
    spawn = spawns[Math.floor(Math.random() * spawns.length)]
    return [listToVec(spawn['loc']), listToVec(spawn['rot'])]
}

function addLight(ldata){
    l = new pc.Entity();
    l.addComponent('light', {
        type: ldata['type'],
        color: new pc.Color(ldata['color'][0], ldata['color'][1], ldata['color'][2]),
        range: ldata['distance'],
        intensity:ldata['energy'],

    });
    l.translate(listToVec(ldata['loc']))
    if (ldata['type'] != 'point'){
        l.setEulerAngles(listToVec(ldata['rot']))
        l.light.outerConeAngle = ldata['angle']
        l.light.innerConeAngle = ldata['angle'] * (1-ldata['blend'])
    }
    app.root.addChild(l)
}

continueloading = true;
