requireshader('energy');
requireobj('Powerup');
requirejs('common.js');

POWERUP_HEALTH = 5

function powerup(){

}

powerup.prototype.collide = function(result){
    if (result.other.name == "Ship"){
        this.entity.destroy();
    }

}

powerup.prototype.init = function(pos, vec){
    pobj = new pc.Entity();
    pobj.name = "Powerup"
    app.systems.model.addComponent(pobj, {
        type: "asset",
        asset: getObj('Powerup.json', "model")
    });
    pobj.translate(pos);
    pobj.addComponent('collision', {type:'sphere', radius:0.2});
    pobj.addComponent('rigidbody', {type:'dynamic',
                                    mass:0.2,
                                    friction:0,
                                    linearDamping:0.6,
                                    group:POWERUP_GROUP,
                                    mask:(pc.BODYGROUP_STATIC | DRONE_GROUP | PLAYER_GROUP),
                                    });
    pobj.rigidbody.syncEntityToBody();
    app.root.addChild(pobj);

    pobj.rigidbody.angularVelocity = getRandomVect(5);
    pobj.rigidbody.applyForce(vec);
    pobj.collision.on('collisionstart', this.collide);

    //Shader
    diffuseMap = pobj.model.model.getMaterials()[0].diffuseMap;
    material = new pc.Material();
    material.blendType = pc.BLEND_ADDITIVEALPHA
    material.depthTest = 1
    material.depthWrite = 0
    material.setShader(getShader('energy'));
    material.setParameter('uDiffuseMap', diffuseMap);
    pobj.model.model.meshInstances.forEach(function (mesh) {
        mesh.material = material;
    });

    this.obj = pobj;


}

function makePowerup(pos, vec){
    p = new powerup();
    p.init(pos, vec);
    return p;
}

continueloading=true;
