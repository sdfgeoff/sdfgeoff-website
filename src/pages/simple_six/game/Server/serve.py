# Starts a HTTP Server of the client directory, and starts a tracker
import threading
#import SimpleHTTPServer
#import SocketServer
import os
import socket


import tracker

client_dir = '../Web Version'

TRACKER_PORT = 42425
WEB_PORT = 80

hostname = socket.gethostname()
IP = socket.gethostbyname(hostname)

print ("Serving from "+hostname)

def start_tracker():
	'''Starts the tracker'''
	ws = tracker.PyWSock()
	ws.start_server(TRACKER_PORT)#(9876)

#def start_web_server():
	'''Starts the HTTP server so that a client can join by pointing their
	web-browser to this computers IP address'''
##	os.chdir(client_dir)
#	try:
#		Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
#		httpd = SocketServer.TCPServer(("", WEB_PORT), Handler)
#
#		print "serving at port", WEB_PORT
#		httpd.serve_forever()
#	except:
#		print "cannot serve on port 80: you need root permission for this"

track = threading.Thread(target = start_tracker).start()
#web = threading.Thread(target = start_web_server).start()

raw_input()
 
for thread in threading.enumerate():
	if (thread.isAlive() and thread.name != 'MainThread'):
		thread._Thread__stop()

