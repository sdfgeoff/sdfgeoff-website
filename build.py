import os
import shutil
import json
import datetime
#import PIL

from collections import namedtuple


ROOT_DIRECTORY = os.path.dirname(os.path.abspath(__name__))

config = {
    "input_folder": os.path.join(ROOT_DIRECTORY, "src"),
    "output_folder": os.path.join(ROOT_DIRECTORY, "bin"),
    "template_path": "src/template.html",
    "thumbnail_min_size": 500, # The smallest side of the image will be this number of pixels
    "rss_num_items": 10,
    "rss_title": "Sdfgeoff's Site",
    "rss_description": "A place where I upload articles, write down the things I think about, and pictures of things I've made",
    "host": "http://www.sdfgeoff.space"
}




PageRef = namedtuple("PageRef", ["url", "name", "description", "date", "icon"])
RawPageInfo = namedtuple("RawPageInfo", ["tags", "date", "content"])



TAGS = {}  # Links from a tag name to a list of PageRef
ALL = []



def get_between(start_str, end_str, content, inclusive=False):
    start_index = content.find(start_str)
    end_index = content.find(end_str, start_index + len(start_str))
    if inclusive:
        end_index += len(end_str)
    else:
        start_index += len(start_str)
    return content[start_index:end_index]



def generate_thumbnail(config, input_image_abs_path):
    filename, ext = os.path.splitext(input_image_abs_path)
    output_image_path = filename + "-thumb" + ext
    assert os.path.exists(input_image_abs_path)
    
    if not os.path.exists(output_image_path):
        pass
        #print("Generating Thumbnail", output_image_path)
        #open(output_image_fullpath, "wb").write(b"Test")
    
    return output_image_path




def generate_page_from_info(config, filepath):
    raw_data = json.load(open(filepath))
    data = RawPageInfo(**raw_data)
    
    base_dir = os.path.dirname(filepath)
    all_content = "\n".join([
        open(os.path.join(base_dir, c)).read() 
        for c in data.content
    ])
    
    rel_path = os.path.dirname(os.path.relpath(filepath, config["input_folder"]))
    rel_output_name = os.path.join(rel_path, "index.html")
    output_fullpath = os.path.join(config["output_folder"], rel_output_name)
    inv_rel_path = os.path.dirname(os.path.relpath(config["input_folder"], filepath))
    stylesheet = os.path.join(inv_rel_path, "style.css")
    
    
    first_heading1 = get_between("<h1>", "</h1>", all_content)
    first_paragraph = get_between("<p>", "</p>", all_content)
    first_image = get_between("<img", ">", all_content)
    first_image_url = get_between('src="', '"', first_image)
    
    all_content = all_content.replace(
        get_between("<h1>", "</h1>", all_content, inclusive=True),
        ""
    )
    
    first_image_rel_url = os.path.join(rel_path, first_image_url)
    if first_image_url == "":
        first_image_rel_url = "images/misc.jpg"

    description = first_paragraph

    date = datetime.datetime.strptime(data.date, '%Y-%m-%d')
    
    generate_page(
        rel_output_name,
        first_heading1,
        first_image_rel_url,
        description,
        all_content,
        date,
        data.tags
    )



def copy_file(config, filepath):
    rel_path = os.path.relpath(filepath, config["input_folder"])
    output_fullpath = os.path.join(config["output_folder"], rel_path)
    
    if not os.path.exists(os.path.dirname(output_fullpath)):
        os.makedirs(os.path.dirname(output_fullpath))
    
    shutil.copy(filepath, output_fullpath)
    



def generate_page(output_relpath, title, icon, description, content, date, tags):
    # Figure out some paths
    output_fullpath = os.path.join(config["output_folder"], output_relpath)
    output_folder_path = os.path.dirname(output_fullpath)
    inv_rel_path = os.path.dirname(os.path.relpath(config["output_folder"], output_fullpath))
    stylesheet = os.path.join(inv_rel_path, "style.css")
    
    # Create an immutable object that can be used to point at this page
    # in future
    page_ref = PageRef(
        name=title,
        date=date,
        description=description,
        url=output_relpath,
        icon=icon,
    )
    
    # Add to tagmap
    if tags == []:
        tags.append("untagged")
    for tag in tags:
        existing_tags = TAGS.get(tag, list())
        existing_tags.append(page_ref)
        TAGS[tag] = existing_tags
    ALL.append(page_ref)
    
    # Do the templating
    tag_links = ['<a href="{}">{}</a>'.format(
        os.path.join(inv_rel_path, "tags/{}.html".format(t)),
        t,
    ) for t in tags]
    
    out_page = config["template_string"].format(
        title=title,
        stylesheet=stylesheet,
        home=os.path.join(inv_rel_path, "index.html"),
        content=content,
        date=date,
        tags=", ".join(tags),
        tag_links = " ".join(tag_links)
    )
    
    # Thumbnail images
    # ~ start = 0
    # ~ found_image = True
    # ~ while found_image:
        # ~ image_tag = get_between("<img", ">", out_page[start:], True)
        # ~ if image_tag == "":
            # ~ found_image = False
        # ~ else:
            
        
            # ~ image_path = get_between("src=\"", '"', image_tag)
            # ~ full_image_path = os.path.join(output_folder_path, image_path)
            # ~ assert os.path.exists(full_image_path)
            
            # ~ thumbnail_abs_path = generate_thumbnail(config, full_image_path)
            # ~ thumbnail_rel_path = os.path.relpath(thumbnail_abs_path, output_folder_path)
            # ~ print(image_path, thumbnail_rel_path)
            
            # ~ new_image_tag = image_tag.replace(image_path, thumbnail_rel_path)
            # ~ start = out_page.find(image_tag, start) + len(new_image_tag)
            
            # ~ out_page = out_page.replace(image_tag, new_image_tag)
    
    # Write it to disk
    out_directory = os.path.dirname(output_fullpath)
    if not os.path.exists(out_directory):
        os.makedirs(out_directory)
    open(output_fullpath, "w").write(out_page)


def generate_tagmap_page(config, output_relpath, pages, title):
    output_fullpath = os.path.join(config["output_folder"], output_relpath)
    inv_rel_path = os.path.dirname(os.path.relpath(config["output_folder"], output_fullpath))
    
    
    all_content = ""
    
    pages.sort(key=lambda x:x.date, reverse=True)
    done_pages = []
    
    newest_year = 0
    
    for page in pages:
        if page in done_pages:
            continue
        done_pages.append(page)
            
        if newest_year != page.date.year:
            newest_year = page.date.year
            all_content += '<h2>{}</h2>\n'.format(newest_year)
        
        rel_url = os.path.join(inv_rel_path, page.url)
        description = page.description
        if len(description) > 240:
            description = description[:236] + '<a href="{}">...</a>'.format(rel_url)

        all_content += """
        <a class="card float-left" href="{url}">
            <img src="{icon}" class="card-background"/>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="-40 -40 80 80" class="card-aperture"><polygon points="0,0 86.67,50 0,100" class="aperture-blade" /><polygon points="0,0 86.67,-50 86.67,50" class="aperture-blade" /><polygon points="0,0 0,-100 86.67,-50" class="aperture-blade" /><polygon points="0,0 -86.67,-50 0,-100" class="aperture-blade" /><polygon points="0,0 -86.67,50 -86.67,-50" class="aperture-blade" /><polygon points="0,0 0,100 -86.67,50" class="aperture-blade" /></svg>
        </a>\n""".format(
            url=rel_url,
            icon=os.path.join(inv_rel_path, page.icon),
            name=page.name
        )
        all_content += """
        <a href="{url}"><b>{title} - {date}</b></a>
        <p>{description}</p>
        
        <div class="float-clear"></div>
        """.format(
            url=os.path.join(inv_rel_path, page.url),
            title=page.name,
            date=page.date.strftime("%Y-%m-%d"),
            description=description
        )
    
    
    generate_page(
        output_relpath,
        title,
        "images/tagmapicon.jpg",
        "",
        all_content,
        datetime.datetime.now(),
        ["tagmap"]
    )


def get_pages_for_tags(*tags):
    pages = []
    for t in tags:
        pages += TAGS.get(t, list())
    return pages


def format_date(date):
    # Mon, 30 Sep 2002 11:00:00 GMT
    return date.strftime("%a, %d %b %Y %H:%M:%S GMT")

def generate_rss_feed(config, output_path, pages):
    pages.sort(key=lambda x:x.date, reverse=True)
    items = pages[:config["rss_num_items"]]
    
    out = '<rss xmlns:blogChannel="http://backend.userland.com/blogChannelModule" version="2.0">'
    out += '<channel>'
    
    out += '<title>{}</title>'.format(config["rss_title"])
    out += '<description>{}</description>'.format(config["rss_description"])
    out += '<link>{}</link>'.format(config["host"])
    
    out += "<lastBuildDate>{}</lastBuildDate>".format(format_date(datetime.datetime.now()))
    
    # LastBuildData
    
    for page in items:
        out += "\n"
        link = os.path.join(config["host"], page.url)
        out += "<item>"
        
        out += "<title>{}</title>".format(page.name)
        out += "<description>{}</description>".format(page.description)
        out += "<link>{}</link>".format(link)
        out += "<pubDate>{}</pubDate>".format(format_date(page.date))
        
        out += '<guid isPermaLink="true">{}</guid>'.format(link)
        
        out += "</item>"
    
    out += "\n"
    out += '</channel>'
    out += '</rss>'
    
    open(os.path.join(config["output_folder"], output_path), "w").write(out)
        


def build(config):
    config["template_string"] = open(config["template_path"]).read()
    
    for root, dirs, files in os.walk(config["input_folder"]):
        for filename in files:
            full_filepath = os.path.join(root, filename)
            if filename in FILE_HANDLERS:
                FILE_HANDLERS[filename](config, full_filepath)
            else:
                copy_file(config, full_filepath)
    
    
    # Generate sitemap and RSS feed of non-generated pages
    generate_rss_feed(config, "rss.xml", ALL)
    generate_tagmap_page(config, "sitemap.html", ALL, "Sitemap")
    
    # Generate a page for each tag
    TAGS["tagmap"] = list()
    for tag in TAGS.keys():
        generate_tagmap_page(config, "tags/{}.html".format(tag), TAGS[tag], "Tag: {}".format(tag))
    
    # Generate some specific pages for linking from the homepage
    generate_tagmap_page(config, "3dprinting-and-robotics.html", get_pages_for_tags("3d_print", "3dprinting", "robotics"), "3d Printing and Robotics")
    generate_tagmap_page(config, "games-and-software.html", get_pages_for_tags("game", "games", "software"), "Games and Software")
    generate_tagmap_page(config, "workshop.html", get_pages_for_tags("workshop"), "Workshop")
    generate_tagmap_page(config, "articles.html", get_pages_for_tags("article", "articles"), "Articles")
    generate_tagmap_page(config, "blog.html", get_pages_for_tags("blog"), "Blog")
    
    generate_tagmap_page(config, "gallery.html", get_pages_for_tags("art", "photos"), "Art and Photos")
    
    
    
    
    
    



FILE_HANDLERS = {
    "info.json": generate_page_from_info
}


build(config)
