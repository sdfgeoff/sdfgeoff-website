import datetime
import json
import os


import tkinter as tk
from tkinter import messagebox

PAGES_DIR = "src/pages"


VALID_SLUG_CHARS = "abcdefghijklmnopqrstuvwxyz_1234567890"


def fixchar(c):
    if c not in VALID_SLUG_CHARS:
        return "_"
    return c


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.heading_contents = tk.StringVar()
        self.heading_contents.set("Heading")
        
        self.heading = tk.Entry(self, textvariable=self.heading_contents)
        self.heading.pack()
        
        
        self.tag_contents = tk.StringVar()
        self.tag_contents.set("blog,")
        self.tags = tk.Entry(self, textvariable=self.tag_contents)
        self.tags.pack()
        
        
        self.date_contents = tk.StringVar()
        self.date_contents.set(datetime.datetime.now().strftime("%Y-%m-%d"))
        self.date = tk.Entry(self, textvariable=self.date_contents)
        self.date.pack()
        
        
        self.page_contents = tk.Text(self)
        self.page_contents.insert('1.0', "<p></p>")
        self.page_contents.pack()
        
        self.save = tk.Button(self, text="SAVE", command=self.save)
        self.save.pack(side="bottom")

    
    def save(self):
        page_name = self.heading.get()
        page_date = self.date.get()
        tags = [t.strip() for t in self.tags.get().split(',')]
        tags = [t for t in tags if t]
        
        contents = self.page_contents.get("1.0", "end")
        
        slug = "".join([
            fixchar(c) for c in page_name.lower()
        ])
        
        output_folder = os.path.join(PAGES_DIR, slug)
        json_file = os.path.join(output_folder, "info.json")
        main_file = os.path.join(output_folder, "main.html")
        
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        else:
            messagebox.showerror("Err", "Page '{}' Exists".format(slug))
            return
            
        
        full_contents = "<h1>{}</h1>\n{}".format(page_name, contents)
        
        json.dump(
            {
                "tags": tags,
                "date": page_date,
                "content": [
                    "main.html"
                ]
            },
            open(json_file, "w"),
            indent=4
        )
        
        open(main_file, "w").write(full_contents)
        
        self.master.destroy()
        

        

root = tk.Tk()
app = Application(master=root)
app.mainloop()
